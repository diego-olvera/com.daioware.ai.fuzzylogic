/**
 * 
 */
package com.daioware.ai.fuzzyLogic;
	
/**
 * @author Diego Olvera
 *
 */
public final class FuzzyLibrary {
	
	public static double trapezeOpenOnTheRight(double u,double a,double b ){
		if(u>b)return 1;
		if(u<a)return 0;
		if(a<=u && u<=b) return (u-a)/(b-a);
		return -1;
	}
	
	public static double trapezeOpenOnTheLeft(double u,double a,double b ){
		if(u>b)return 0;
		if(u<a)return 1;
		if(u>=a && u<=b) return (b-u)/(b-a);
		return -1;
	}

	public static double triangular(double u,double a,double b,double c){
		if(u<a || u>c)return 0;
		if(a<=u && u<b)return (u-a)/(b-a);
		if(b<=u && u<=c) return (c-u)/(c-b);
		return -1;
	}
	
	public static double trapezoidal(double u,double a,double b,double c,double d){
		if(u<a || u>d)return 0;
		if(u>=b && u<=c)return 1;
		if(u>=a && u<b)return (u-a)/(b-a);
		if(c<u && u<=d) return (d-u)/(d-c);
		return -1;
	}

	public static double sCurve(double u,double a,double b){
		if(u>b)return 1.0;
		if(u<a)return 0.0;
		if(a<=u && u<=b)return (1+Math.cos(((u-b)/(b-a))*Math.PI))/2.0;
		return -1;
	}
	
	public static double zCurve(double u,double a,double b){
		if(u>b)return 0.0;
		if(u<a)return 1.0;
		if(a<=u && u<=b) return  (1+Math.cos(((u-a)/(b-a))*Math.PI))/2.0;
		return -1;
	}
	
	public static double softTriangular(double u,double a,double b,double c){
		if(u<a || u>c) return 0.0;
		if(a<=u && u<b) return (1+Math.cos(((u-b)/(b-a))*Math.PI))/2.0;
		if(b<=u && u<=c) return (1+Math.cos(((b-u)/(c-b))*Math.PI))/2.0;
		return -1;
	}
	
	public static double softTrapezoidal(double u,double a,double b,double c,double d){
		if(u<a || u>d)return  0.0;
		if(b<=u && u<=c) return 1.0;
		if(a<=u && u<b) return (1+Math.cos(((u-b)/(b-a))*Math.PI))/2.0;
		if(c<u && u<=d) return (1+Math.cos(((c-u)/(d-c))*Math.PI))/2.0;
		return -1;
	}
	
	private static double min(double a,double b){
		return Double.min(a, b);
	}
	
	private static double max(double a,double b){
		return Double.max(a, b);
	}
	
	public static double compAND(double ma_u,double mb_u){
		return min(ma_u,mb_u);
	}
	
	public static double compOR(double ma_u,double mb_u){
		return max(ma_u,mb_u);
	}
	
	public static double negate(double ma_u){
		return 1.0-ma_u;
	}
	
	public static double zadehImplicate(double ma_x,double mb_y){
		return max(min(ma_x,mb_y),negate(ma_x));
	}
	
	public static double mamdaniImplicate(double ma_x,double mb_y){
		return min(ma_x,mb_y);
	}
	
	public static double godelImplicate(double ma_x,double mb_y){
		if(ma_x<=mb_y)return  1;
		else return mb_y;
	}
	
}
